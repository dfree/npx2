<div class="d-nav">
  <nav>
    <div class="banner"><img id="banner" src="img/header-blank-banner.png"></div>
    <div class="menu">
      <a href="#">Buy Your Tickets</a>
      <a href="#" class="contact-btn">Contact Us</a>
      <a href="#">Lodging</a>
      <a href="#">In the press</a>
      <a href="#">NPX Store</a>
      <a href="#">Find a portal location</a>
      <?php include('contact.php') ?>
    </div>
  </nav>
</div>

<div class="m-nav">
  <div class="m-nav-btn">
      <div class="icon-bar bar-top"></div>
      <div class="icon-bar bar-mid"></div>
      <div class="icon-bar bar-low"></div>
  </div>
  <nav>
    <a href="#"><div class="item">Buy Your Tickets</div></a>
    <a href="#"><div class="item">Contact Us</div></a>
    <a href="#"><div class="item">Lodging</div></a>
    <a href="#"><div class="item">In the press</div></a>
    <a href="#"><div class="item">NPX Store</div></a>
    <a href="#"><div class="item">Find a portal location</div></a>
    <div class="social">
      <div class="facebook"><img src="img/header-facebook.svg"></div>
      <div class="twitter"><img src="img/header-twitter.svg"></div>
      <div class="instagram"><img src="img/header-instagram.svg"></div>
      <div class="pintrest"><img src="img/header-pintrest.svg"></div>
    </div>
  </nav>
</div>