<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->

        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.0.min.js"></script>

        <link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic|Josefin+Sans:700|Libre+Baskerville|Tienne|Muli|Oswald:300|Merriweather:300,400|Adamina|Abril+Fatface' rel='stylesheet' type='text/css'>

