<div class="testimonial-content">
  <h1>Testimonials</h1>
  <div class="close-btn"></div>
  <div class="lines"></div>
  <div class="testimonial-section">
    <div class="single-testimonial">
      <h3>Melissa R. - </h3>
      <p>This was the BEST family vacation we had taken at Xmas time! My kids had the most amazing time from getting their Golden Tickets, to riding the trolley to the North Pole, then making toys for the children and signing with the elves while enjoying Snowman Soup and cookies.. Then the final adventure seeing Santa was the highlight!!! Such a fun exciting time our whole family had from beginning to end, Well done NPX!!! Thanks for the memories.</p>
    </div>
    <div class="single-testimonial">
      <h3>Leslie Bechtold B. -</h3>
      <p>Took my 4 yr old daughter who has not stopped talking about the experience. It was so incredible to see her eyes ‘light up’ and she absolutely LOVED every minute of the experience. It brought tears to my eyes to see the joy in my daughter.</p>
    </div>
    <div class="single-testimonial">
      <h3>Kelli S. - </h3>
      <p>We had an absolute blast two years ago in Greer. We cannot wait to take our two up to Flagstaff. It’s such a magical place.</p>
    </div>
  </div>
  <div class="separator"></div>
  <div class="testimonial-section">
    <div class="single-testimonial">
      <h3>Denise I. -</h3>
      <p>I had dreamed of a place like this to take my child and they surpassed all of our expectations. I wish we could make the trip every year!</p>
    </div>
    <div class="single-testimonial">
      <h3>Tiffany T. - </h3>
      <p>The North Pole experience is a magical place. You feel like you are transported directly to the North Pole. The gleam in the children’s eyes when they realize they are visiting Santa’s Workshop is priceless. Their faces beam as they bounce from room to room. Being able to explore all the North Pole Experience has to offer is truly an event that all families should experience and one that children will never forget.</p>
    </div>
    <div class="single-testimonial">
      <h3>Julia L. -</h3>
      <p>My husband and I took our 6 children to the NPX and had the most memorable Christmas. It was like being a part of a magical Christmas story.</p>
    </div>
  </div>
</div>

