<section class="press">
  <div class="lines"></div>
  <h1>NPX in the Press</h1>
  <ul>
    <li class="mentions-btn active">Mentions</li>
    <li class="press-kit-btn">Press Kit</li>
    <li class="media-ready-btn">Media Ready Photos</li>
    <li class="video-btn">Media Ready Videos</li>
  </ul>

  <div class="press-content cycle-slideshow"
  data-cycle-fx='fade'
  data-cycle-timeout='0'
  data-cycle-slides="> div"
  >

  <div class="mentions">
    <div class="mentions-grid">
      <div class="mentions-hero">
        <div class="close-button"></div>
        <div class="h-logo"></div>
        <div class="h-text"></div>
        <a href="#" target="_blank"><div class="h-link"><p>View Article</p></div></a>
      </div>

      <div class="mentions-cycle cycle-slideshow"
      data-cycle-fx="scrollHorz"
      data-cycle-timeout='0'
      data-cycle-slides="> span"
      data-cycle-pager=".numbers"
      data-cycle-pager-template="<strong><a href=#> {{slideNum}} </a></strong>"
      data-cycle-prev=".prev"
      data-cycle-next=".next"
      >
      <span>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">one. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/1" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">two. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/2" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">three/ Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/3" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">four. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/4" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">five. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/5" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">six. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/6" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">seven. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/7" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">eight. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/8" target="_blank"><div class="m-link"></div></a>
        </div>
      </span>
      <span>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">one. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/1" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">two. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/2" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">three/ Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/3" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">four. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/4" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">five. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/5" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">six. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/6" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">seven. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/7" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">eight. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/8" target="_blank"><div class="m-link"></div></a>
        </div>
      </span>
      <span>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">one. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/1" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">two. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/2" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">three/ Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/3" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">four. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/4" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">five. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/5" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">six. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/6" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">seven. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/7" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">eight. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/8" target="_blank"><div class="m-link"></div></a>
        </div>
      </span>
      <span>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">one. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/1" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">two. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/2" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">three/ Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/3" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">four. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/4" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">five. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/5" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">six. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/6" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">seven. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/7" target="_blank"><div class="m-link"></div></a>
        </div>
        <div class="mentions-box">
          <div class="m-logo"><img src="img/press-logo.png"></div>
          <div class="m-text">eight. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed pulvinar felis, a malesuada metus. Quisque feugiat lacus vitae dolor congue, eget rutrum leo fermentum. Donec condimentum nibh odio. Pellentesque et risus eros ut metus sem.</div>
          <a href="http://www.azfamily.com/8" target="_blank"><div class="m-link"></div></a>
        </div>
      </span>
      </div>
    </div>
    <div class="pager">
      <div class="numbers"></div>
      <div class="next"></div>
      <div class="prev"></div>
    </div>
  </div> <!-- END MENTIONS -->

  <div class="press-kit">
    <div class="press-grid">
      <div class="section">
        <h3>Press Kit</h3>
        <span>--</span>
        <p class="special">Welcome to the North Pole Experience virtual press kit. This site features the most up-to-date information about NPX. If you are unable to locate what you are looking for, contact us directly.</p>
        <a href="#" target="_blank"><p>Download Press Kit PDF<p></a>
      </div>
      <div class="separator"></div>
      <div class="section">
        <h3>Press Releases</h3>
        <span>--</span>
        <a href="#" target="_blank"><p>Press Release Title and Link Here 1</p></a>
        <a href="#" target="_blank"><p>Press Release Title and Link Here 2</p></a>
        <a href="#" target="_blank"><p>Press Release Title and Link Here 3</p></a>
        <a href="#" target="_blank"><p>Press Release Title and Link Here 4</p></a>
        <a href="#" target="_blank"><p>Press Release Title and Link Here 5</p></a>
        <a href="#" target="_blank"><p>Press Release Title and Link Here 6</p></a>
        <a href="#" target="_blank"><p>Press Release Title and Link Here 7</p></a>
        <a href="#" target="_blank"><p>Press Release Title and Link Here 8</p></a>
      </div>
      <div class="separator"></div>
      <div class="section">
        <h3>NPX at a Glance</h3>
        <span>--</span>
        <a href="#" target="_blank"><p>Backgrounder</p></a>
        <a href="#" target="_blank"><p>Fact Sheet</p></a>
        <a href="#" target="_blank"><p>Q&amp;A</p></a>
        <a href="#" target="_blank"><p>Media Visits Protocol</p></a>
      </div>
    </div>
  </div>
  <div class="media-ready">
    <div class="media-ready-grid">
      <div class="media-ready-hero">
        <div class="close-button"></div>
        <a href="#" download="npx-press-image"><div class="download"></div></a>
      </div>
      <div class="media-ready-box"><img src="img/slide-1.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-2.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-3.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-4.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-5.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-6.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-7.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-8.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-9.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-10.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-11.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-12.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-13.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-14.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-15.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-16.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-17.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-1.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-2.jpg"></div>
      <div class="media-ready-box"><img src="img/slide-3.jpg"></div>
    </div>

  </div>
  <div class="video-content">PRESS READY VIDEOS</div>
    
  </div>

</section>