<div class="lodging-content">
  <div class="close-btn"></div>
  <h1>Choose Your Lodging</h1>
  
  <div class="property-cycle cycle-slideshow"
      data-cycle-fx="fade"
      data-cycle-timeout='0'
      data-cycle-slides="> span"
      data-cycle-pager=".lodging-numbers"
      data-cycle-pager-template="<strong><a href=#> {{slideNum}} </a></strong>"
      data-cycle-prev=".lodging-prev"
      data-cycle-next=".lodging-next"
      >
    
    <span>
      <div class="lodging-col">
        <div class="property-image"><img src="img/location-littleAmerica.jpg"></div>
      </div>
      <div class="separator"></div>
      <div class="lodging-col">
        <h3>Little America NPX Trolley Station</h3>
        <p>Address: <span>2515 E. Butler Avenue, Flagstaff, AZ 86004</span></p>
        <p>Reservations: <span>1-888-469-8819</span></p>
        <p>Website: <a href="http://flagstaff.littleamerica.com/promotions/north-pole-experience" target="_blank">Little America NPX Trolley Station</a></p>
        <br />
        <p>NPX has partnered with another premier holiday and Christmas tradition in Arizona; The Little America Hotel, which is decked out in over one-million lights over the holidays and is the host as Santa’s trolley launch station. All trolleys will depart to the workshop from Little America, as well as dinner and Breakfast with Santa.</p>
      </div>
    </span>

    <span>
      <div class="lodging-col">
        <div class="property-image"><img src="img/location-littleAmerica.jpg"></div>
      </div>
      <div class="separator"></div>
      <div class="lodging-col">
        <h3>DOUBLETREE BY HILTON</h3>
        <p>Address: <span>Address: 1175 W Route 66, Flagstaff, AZ, 86001</span></p>
        <p>Reservations: <span> 928-773-8888</span></p>
        <p>Website: <a href="https://secure.hilton.com/en/dt/res/choose_dates.jhtml?hotel=FLGSSDT&spec_p" target="_blank">DOUBLETREE BY HILTON</a></p>
        <br />
        <p>The Doubletree by Hilton is located right off ‘old route 66′, near historic downtown Flagstaff and the Flagstaff airport, featuring contemporary, spacious rooms, relaxing resort amenities, a warm and welcoming ambience. Their NPX package is a $99.00 rate and includes two breakfast vouchers, hot cocoa, cookies and directions to our trolley departure station in your room.</p>
      </div>
    </span>

    <span>
      <div class="lodging-col">
        <div class="property-image"><img src="img/location-littleAmerica.jpg"></div>
      </div>
      <div class="separator"></div>
      <div class="lodging-col">
        <h3>COURTYARD MARRIOTT FLAGSTAFF</h3>
        <p>Address: <span>2515 E. Butler Avenue, Flagstaff, AZ 86004</span></p>
        <p>Reservations: <span>1-888-469-8819</span></p>
        <p>Website: <a href="http://flagstaff.littleamerica.com/promotions/north-pole-experience" target="_blank">Little America NPX Trolley Station</a></p>
        <br />
        <p>NPX has partnered with another premier holiday and Christmas tradition in Arizona; The Little America Hotel, which is decked out in over one-million lights over the holidays and is the host as Santa’s trolley launch station. All trolleys will depart to the workshop from Little America, as well as dinner and Breakfast with Santa.</p>
      </div>
    </span>

    <span>
      <div class="lodging-col">
        <div class="property-image"><img src="img/location-littleAmerica.jpg"></div>
      </div>
      <div class="separator"></div>
      <div class="lodging-col">
        <h3>HOLIDAY INN EXPRESS FLAGSTAFF</h3>
        <p>Address: <span>2515 E. Butler Avenue, Flagstaff, AZ 86004</span></p>
        <p>Reservations: <span>1-888-469-8819</span></p>
        <p>Website: <a href="http://flagstaff.littleamerica.com/promotions/north-pole-experience" target="_blank">Little America NPX Trolley Station</a></p>
        <br />
        <p>NPX has partnered with another premier holiday and Christmas tradition in Arizona; The Little America Hotel, which is decked out in over one-million lights over the holidays and is the host as Santa’s trolley launch station. All trolleys will depart to the workshop from Little America, as well as dinner and Breakfast with Santa.</p>
      </div>
    </span>
    
    <div class="pager">
      <div class="lodging-numbers"></div>
      <div class="lodging-next"></div>
      <div class="lodging-prev"></div>
    </div>
  </div>
</div>
