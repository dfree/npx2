<div class="infograpghs-cycle cycle-slideshow"
data-cycle-fx=scrollHorz
data-cycle-timeout=10000
data-cycle-slides="> div"
>

<div class="temp">
  <h3>Temp At the North Pole</h3>
  <p class="npTemp"></p>
</div>
<div class="days">
  <h3>Days Until Christmas</h3>
  <div class="countdown"></div>
  <h3>Mark your calendars</h3>
</div>

</div>