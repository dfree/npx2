<form>
  <div class="form-section form-01 form-active">
    <h3>Hello.</h3>
    <p>What is your name?</p>
    <input id="fn" placeholder="First &amp; Last Name" autocorrect="off" type="text"><br />
  </div>
  <div class="form-section form-02">
    <h3>Thanks.</h3>
    <p>What is your email?</p>
    <input id="fn" placeholder="Email Address" autocorrect="off" type="text"><br />
  </div>
  <div class="form-section form-03">
    <h3>Alomst Done.</h3>
    <p>What is your phone?</p>
    <input id="fn" placeholder="Phone Number" autocorrect="off" type="text"><br />
  </div>
  <div class="form-section form-04">
    <h3>One More.</h3>
    <p>What is your address?</p>
    <input id="fn" placeholder="Address" autocorrect="off" type="text"><br />
  </div>
  <div class="form-section form-05">
    <h3>Last One.</h3>
    <p>What is your city, state and zip?</p>
    <input id="fn" placeholder="City, State &amp; Zip" autocorrect="off" type="text"><br />
  </div>
  <div class="form-section form-thanks">
    <h3>Thank You!</h3>
    <br />
    <p>We'll keep you up to date as long as you stay off the naughty list.</p>
  </div>
    <span class="q-no">(question no. 1/5)</span>
    <div class="next-btn"><p>Next</p></div>
    <div class="submit-btn"><p>Submit</p></div>
  </div>
</form>