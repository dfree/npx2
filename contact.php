<section class="contact-drop">
  <div class="close-button"></div>
      <div><p>Email<a href="mailto:info@northpoleexperience.com">info@northpoleexperience.com</a></p></div>
      <div><p>Phone:</p><a href="tel:4807799679">480.779.9679</a></div>
      <div><p>Media Contact:</p><a href="mailto:media@northpoleexperience.com">media@northpoleexperience.com</a></div>
</section>