<?php include('head.php') ?>
<title>The Northpole Experience</title>
</head>
    <body>
        <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    
    <?php include('header.php') ?>
    <section class="hero">
      <div class="border"></div>
      <video id="bgVid" loop autoplay>
        <source src="video/hero-bgVid-1080.webm" type="video/webm">
        <source src="video/hero-bgVid-1920.mp4" type="video/mp4">
        <source src="video/hero-bgVid-1920.ogv" type="video/ogg">
      </video>
      <div class="content">
        <div class="button">
          <p>Book Your NPX Tickets</p>
          <div class="button-border"></div>
        </div>
      </div>
    </section>

    <section class="grid">

      <div class="top">
        <?php include('testimonials.php') ?>
        <div class="left">
          <div class="tradition">
            <div class="border"></div>
            <div class="content">
              <h1>A Tradition <br /> in the making</h1>
              <div class="doubleLine"></div>
              <p>Delight in the magic of the holiday season at Santa’s enchanting 400-year-old workshop at the North Pole. We’ll take you on a tour through the toy factory, bakery, Elf University, and Santa’s sleigh with St. Nick himself.<br />Let’s get to work, we’ve got toys to build.</p>
              <div class="vimeo">
                <iframe src="http://player.vimeo.com/video/58493550?api=1?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=0&amp;loop=0" id="video1" width="100%" height="100%" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
              </div>
            </div>
          </div>
          <div class="info">
            <?php include('infographs.php') ?>
          </div>
        </div>
        <div class="right">
          <div class="testimonials">
          <div class="border"></div>
            <div class="content">
              <h1>Testimonials</h1>
              <p>We couldn’t do this without you. Sharing in our love for the Christmas season, our loyal guests keep our corner of the North Pole shining bright. Let them tell you in their own words.</p>
              <div class="button">Read For Yourself</div>
            </div>
          </div>
          <div class="gallery">
            <div class="border"></div>
            <div class="content">
              <h1>A Glimpse into <br />Santa’s Workshop</h1>
              <div class="key"></div>
              <p>Santa’s 12,000-square-foot workshop is one impressive site to see. Take a peek at the place before you arrive, we won’t tell the elves. </p>
              <div class="gallery-images cycle-slideshow"
                data-cycle-fx=scrollHorz
                data-cycle-timeout=5000
                data-cycle-pager=".cycle-pager"
                >
                <img src="img/slide-1.jpg">
                <img src="img/slide-2.jpg">
                <img src="img/slide-3.jpg">
                <img src="img/slide-4.jpg">
                <img src="img/slide-5.jpg">
                <img src="img/slide-6.jpg">
                <img src="img/slide-7.jpg">
                <img src="img/slide-8.jpg">
                <img src="img/slide-9.jpg">
                <img src="img/slide-10.jpg">
              </div>
              <div class="cycle-pager"></div>
            </div>
          </div>
        </div>
      </div>

      <div class="mid">
        <div class="connect">
          <div class="border"></div>
          <div class="content">
              <h2>Give Us a Jingle</h2>
              <h1>Connect with us</h1>
              <div class="doubleLine"></div>
              <p>The North Pole Experience</p>
              <a href="#">480-779-9679</a><br />
              <a href="#">info@northpoleexperience.com</a><br />
              <span>-</span>
              <p>Santa's Trolley Launch Station<br />at Little America Hotel</p>
              <a href="#">888-469-8819</a><br />
              <a href="#">flagstaff.littleamerica.com</a><br />
              <span>-</span>
              <p>Media Contact</p>
              <a href="#">media@northpoleexperience.com</a>
              <div class="doubleLine"></div>
              <div class="tag">Contact us today to find out more about<br /> franchising &amp; career opportunities</div>
          </div>
        </div>
        <div class="newsletter">
          <div class="border"></div>
          <div class="content">
            <h1>The NPX Newsletter</h1>
            <p>Now everyone can be on the nice list.<br />Sign up for the North Pole Experience newsletter to stay up-to-date!</p>
            <div class="tag">Any e-mail sent by NPX provide's the option to be removed from the e-mail mailing list.</div>
            <div class="news-form"><?php include('form.php') ?></div>
          </div>
        </div>
      </div>

      <div class="bottom">
        <?php include('what-to-expect.php') ?>
        <?php include('lodging.php') ?>
        <div class="expect">
        <div class="border"></div>
          <div class="content">
            <h1>What to expect</h1>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto.</p>
            <div class="button">See the adventure</div>
          </div>
        </div>
        <div class="gear">
          <div class="content">
            <h1>Get Your NPX Gear</h1>
            <p>Vivamus eu rhoncus metus. Integer turpis purus, vestibulum id rutrum quis, blandit ut lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fameslectus.</p>
            <div class="button">Click for NPX Store</div>
          </div>
        </div>
        <div class="tickets-avail">
        <div class="border"></div>
          <div class="content">
            <h1>2014 Ticket Availability</h1>
            <p>Choose a date and grab your tickets for you and your family’s newest holiday tradition. The kids will thank you for it.</p>
            <div class="button">Book Now</div>
          </div>
        </div>
        <div class="lodging">
          <div class="content">
            <h1>Lodging</h1>
            <div class="lines"></div>
            <p>You don’t have to stray far from the workshop. With a handful of charming lodging choices, like our host hotel Little America, take our trolley back into the North Pole after a good night’s rest.</p>
            <div class="lines"></div>
            <div class="button">View NPX Certified Hotels</div>
          </div>
        </div>
      </div>

    </section>

    <?php include('press.php') ?>
    
    

    <?php include('footer.php') ?>
    <?php include('scripts.php') ?>
    </body>
</html>
