<div class="expect-content">
  <h1>What To Expect</h1>
  <div class="close-btn"></div>
  <!-- <div class="lines"></div> -->
  <div class="expect-col">
    <h3>Frequently Asked Questions</h3>
    <div class="faq-data">
      <p>Are tickets to the North Pole Experience refundable?</p>
      <span>Tickets to the North Pole Experience are NON-REFUNDABLE, but can be extended to another date, time during the 2014 season. Please note, any changes must be pre-arranged three weeks prior to original trolley departure date. There is one courtesy date change on all tickets. All additional changes are $5.00 per guest/ticket. If you do not cancel or reschedule in advance within the above specified time frame, or if you fail to show up for your scheduled tour / time, you will forfeit all monies paid.</span>
    </div>
    <div class="faq-data">
      <p>I have already purchased tickets but I would like to change my current reservation?</p>
      <span>The North Pole Experience offers a one-time courtesy change per reservation and any additional changes are subject to availability. The rebooking fee is $5.00/ticket. Please make sure to plan your trip accordingly when booking. If change is needed, please email info@northpoleexperience.com. Please note, all NPX tickets are non-refundable from time of purchase due to intricate timing and planning of the experience.</span>
    </div>
    <div class="faq-data">
      <p>Does Santa’s Workshop have handicap accommodations?</p>
      <span>Handicap accommodations are available — to make sure you are booked on a handicap accessible trolley, please call 480-779-9679 to speak to a NPX representative to book.</span>
    </div>
    <div class="faq-data">
      <p>What is the difference between day time workshop tours and evening workshop tours?</p>
      <span>DAY: NPX has day-time workshop tours on selected dates with tour times from 10:00-3:45pm. These day time tours  have special trolley effects so children can experience the magic and wonder of Santa’s Workshop without the night time exterior lights.</span>
    </div>
    <div class="faq-data">
      <p>How many people are on each trolley? If I book at the same time/date as another family, how do I ensure I’m in the same group?</p>
      <span>Each trolley holds an intimate group of 50 people. This will be the group you are with the entire experience, so please plan ahead if you are traveling with friends or family. The small groups ensure your child will have lots of one-on-one time with everyone they meet. There is only one trolley that leaves at each time and date, so as long as you book the same time, you will be in the same group together. Private trolley’s can be reserved for your group, with minimum 50 ticket purchase.</span>
    </div>
    <div class="faq-data">
      <p>What is the total duration of the experience? </p>
      <span>The North Pole Experience lasts approximately an hour and 1/2 to hour and 45 minutes from the time of departure from Little America to your arrival back at the hotel. (Please note, this does not include meal time.)</span>
    </div>
  </div>

  <div class="separator"></div>

  <div class="expect-col answer-col">
    <div class="answer">
      <h3>The NPX Adventure</h3>
      <p>For over 400 years, I have been using magic portals to travel the world to deliver toys to children on Christmas Eve, and now, I have opened my most secret portal location in Flagstaff, Arizona that leads families to our massive historic toy-building workshop at the North Pole. On behalf of Mrs. Claus, all my elves, and myself, we can’t wait to see all the wonderful families and children this Christmas season! Let’s get to work! We have toys to build!
      <br /><br />
      With a warm heart,</p>
    </div>
  </div>

</div>