//store all utility files

function Utils(){

  //use console logging or not
  this.useDebug = false;
}

/**
 * Returns nothing, used as switch to turn logging on/off
 *
 * Example: test what plaftform is being used
 * Utils.debuglog("hello world!");
 *
 */
Utils.clog = function(s)
{
  if(this.useDebug)
    console.log(s);
};


/**
 * Returns a boolean that represents the platform
 *
 * Example: test what plaftform is being used
 * Utils.isMobile()
 *
 * @returns true or false
 * @type Boolean
 */
Utils.isMobile = function(){
  return navigator.userAgent.toLowerCase().match(/android|webos|iphone|ipad|ipod|blackberry/);
};

Utils.isFirefox = function(){
  return (/Firefox/i.test(navigator.userAgent));
};


/**
 * Returns a number whose value the current time in milliseconds since 1970
 *
 * Example: check the current time in milliseconds
 * Utils.rightNow()
 *
 * @returns A number
 * @type Number
 */
Utils.rightNow = function()
{
  if (window['performance'] && window['performance']['now']) {
    return window['performance']['now']();
  } else {
    return +(new Date());
  }
};

/**
 * Returns a string with padded zeros in front
 *
 * Example: get padded number back
 * var num = Utils.paddit(5)
 *
 * @returns A string with zero padding to the frames
 * @type String
 */
Utils.paddit = function(ind){
  
  var str = ind;
  if(ind < 10)
    str =  "0000" +ind;
  if(ind < 100 && ind > 9)
    str = "000"+ind;
  if(ind < 1000 && ind > 99)
    str = "00" + ind;

  return str;
}


/**
 * Returns a number whose value is limited to the given range.
 *
 * Example: limit the output of this computation to between 0 and 255
 * Utils.clamp(var, 0, 255)
 *
 * @param {Number} min The lower boundary of the output range
 * @param {Number} max The upper boundary of the output range
 * @returns A number in the range [min, max]
 * @type Number
 */
Utils.clamp = function(oThis, min, max) {
  return Math.min(Math.max(oThis, min), max);
};



