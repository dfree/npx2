/*

  The Northpole Experience v2.0

  Site Development by Kitchen Sink Studios, Inc
  Developer: Daniel Freeman [@dFree]

*/

$(document).ready(function(){ 

// GLOBALS -----------------------
var windowHeight = $(window).height(),
    windowWidth = $(window).width(),
    device = navigator.userAgent.toLowerCase(),
    mobileos = device.match(/android|webos|iphone|ipad|ipod|blackberry/i);


// WINDOW RESIZE -----------------------
function resizeOnChange(){
    //console.log('resizing');
    getSizes();
    $('.left-border, .right-border').css('height', windowHeight);
}
function getSizes(){
  windowHeight = $(window).height();
  windowWidth = $(window).width();
}
$(window).resize(function(){resizeOnChange();});


// CHANGES ON MOBILE -----------------------
if (mobileos){
  //console.log('mobile');
  $('video').remove();
  $('.hero').css('height', windowHeight);
}


// FORM ANIMATIONS -----------------------
var question = '1';
$('.next-btn').click(function(){
    $('.form-active').transition({x:'-400px'}).removeClass('form-active');
    question ++;
    var toActive = (".form-0" + question);
    $('.q-no').text("(question no. " + question + "/5)");
    $(toActive).addClass('form-active');
    $('.form-active').transition({x:'0px'});
    if (question == '5'){
      $('.next-btn').fadeOut(function(){
        $('.submit-btn').fadeIn();
      });
    }
});
$('.submit-btn').click(function(){
  $('.form-active, .q-no, .submit-btn').fadeOut(function(){
    $('.form-thanks').fadeIn();
  });
});


// CREATE AND ANIMATE BORDERS -----------------------
$('.left-border, .right-border').css('height', windowHeight);
$(window).scroll(function(){
    var scrollPos = ($(this).scrollTop()*0.04);
    $('.right-border').css('background-position-y', scrollPos);
    $('.left-border').css('background-position-y', -(scrollPos));
});


// ANIMATE NAV -----------------------
if (!mobileos){
  $('.hero').waypoint(function(direction){
    if(direction == "down"){
      $('.title').transition({height: '55px'}, 600, 'ease');
      $('.tickets').transition({margin: '2px 0px 0px 33px'}, 600, 'ease');
      $('.social').transition({margin: '-54px 33px 0px 0px'}, 600, 'ease');
      $('.hero').transition({margin: '110px auto 0px'}, 600, 'ease');
      $('.logo').transition({width: '0px'}, 200, 'ease', function(){
        $('.logo-small').transition({width: '310px'}, 300, 'ease');
      });
    } else if(direction == "up"){
      $('.title').transition({height: '90px'}, 600, 'ease');
      $('.tickets').transition({margin: '20px 0px 0px 33px'}, 600, 'ease');
      $('.social').transition({margin: '-37px 33px 0px 0px'}, 600, 'ease');
      $('.hero').transition({margin: '145px auto 0px'}, 600, 'ease');
      $('.logo-small').transition({width: '0px'}, 200, 'ease', function(){
        $('.logo').transition({width: '376px'}, 300, 'ease');
      });
    } else {return false;}
  });
}

// ANIMATE MOBILE NAV -----------------------
$('.m-nav-btn').click(function(){
  var clicked = $(this);
  if ($(clicked).hasClass('open')){
    switchToOpen(clicked);
  } else {
    switchToClosed(clicked);
  }
});

function switchToOpen(clicked){
  $(clicked).removeClass('open');
  $('.bar-top').transition({ rotate: '45deg', x: '7', y: '3'});
  $('.bar-mid').transition({rotate: '45deg', x: '2', y: '-2'}).transition({opacity: '0'});
  $('.bar-low').transition({ rotate: '-45deg', x: '7', y: '-3'});
  $('.m-nav').transition({right: "-0px"}, 400, 'ease');
  $('.d-nav nav .banner').transition({margin: "0px 0px 0px 62px"}, 400, 'ease');
  $('.hero, .grid, .press, .location').transition({opacity: "0.2"}, 400, 'ease');
}
function switchToClosed(clicked){
  $(clicked).addClass("open");
  $('.m-nav').transition({right: "-260px"}, 400, 'ease');
  $('.d-nav nav .banner').transition({margin: "0px 0px 0px 2px"}, 400, 'ease');
  $('.hero, .grid, .press, .location').transition({opacity: "1.0"}, 400, 'ease', function(){
    $('.bar-top').transition({ rotate: '0deg', x: '0', y: '0'});
    $('.bar-mid').css({opacity: '1'}).transition({rotate: '0deg', x: '0', y: '0'});
    $('.bar-low').transition({ rotate: '0deg', x: '0', y: '0'});
  });
}




// SHOW/CLOSE CONTACT -----------------------
$('.contact-btn').click(function(event){
  $('.contact-drop').transition({right:'0px'});
  event.preventDefault();
});
$('.contact-drop .close-button').click(function(){
  $('.contact-drop').transition({right:'-100%'});
});


// SHOW/CLOSE TESTIMONIALS -----------------------
$('.testimonials .button').click(function(){
  var totalHeight = 125;
  $('.testimonial-content').fadeIn();
  if(windowWidth < 768){
    $(".testimonial-section").each(function(index){
        totalHeight += $(this).outerHeight();
    });
    $('.left').transition({marginLeft: '-100%'});
    $('.right').transition({marginLeft: '-100%'});
    $('.top').css('height', totalHeight);
  }else{
    $('.left').transition({marginLeft: '-60%'});
    $('.right').transition({marginRight: '-60%'});
  }
  $('html, body').animate({scrollTop: $('.top').offset().top-110}, 800,'easeInOutExpo', function(){
      
  });
});

$('.testimonial-content .close-btn').click(function(){
  if(windowWidth < 768){
    $('.left').transition({marginLeft: '0%'});
    $('.right').transition({marginLeft: '0%'}, function(){
      $('.testimonial-content').fadeOut();
      $('.top').css('height', "auto");
    });
  }else{
    $('.left').transition({marginLeft: '0%'});
    $('.right').transition({marginRight: '0%'}, function(){
      $('.testimonial-content').fadeOut();
      $('.top').css('height', "auto");
    });
  }
});


// SHOW/CLOSE EXPECT -----------------------
$('.expect .button').click(function(){
  $('.bottom').css('background-color', '#a4854b');
  $('.expect, .tickets-avail').transition({x: '-100%'});
  $('.gear, .lodging').transition({x: '100%'});
  $('.expect-content').fadeIn(0);
  $('html, body').animate({scrollTop: $('.bottom').offset().top-110}, 800,'easeInOutExpo', function(){
      $('.close-btn').fadeIn();
  });
});

$('.expect-content .close-btn').click(function(){
    $('.expect, .tickets-avail').transition({x: '0%'});
    $('.gear, .lodging').transition({x: '0%'}, function(){
      $('.expect-content, .close-btn').fadeOut(0);
    });
});


// SHOW/CLOSE LODGING -----------------------
$('.lodging .button').click(function(){
  $('.bottom').css('background-color', '#282828');
  $('.expect, .tickets-avail').transition({x: '-100%'});
  $('.gear, .lodging').transition({x: '100%'});
  $('.lodging-content').fadeIn(0);
  $('html, body').animate({scrollTop: $('.bottom').offset().top-110}, 800,'easeInOutExpo', function(){
      $('.close-btn').fadeIn();
      if (windowWidth < 768){
        $('.bottom').css('height', "800px");
      }
  });
});

$('.lodging-content .close-btn').click(function(){
    if (windowWidth < 768){
      $('.bottom').css('height', "100%");
    }
    $('.expect, .tickets-avail').transition({x: '0%'});
    $('.gear, .lodging').transition({x: '0%'}, function(){
      $('.lodging-content, .close-btn').fadeOut(0);
    });
});



// PRESS CYCLE BUTTONS -----------------------
$('.mentions-btn').click(function(){
  $('.press-content').cycle('goto', 0);
  setActive($(this));
});
$('.press-kit-btn').click(function(){
  $('.press-content').cycle('goto', 1);
  setActive($(this));
});
$('.media-ready-btn').click(function(){
  $('.press-content').cycle('goto', 2);
  setActive($(this));
});
$('.video-btn').click(function(){
  $('.press-content').cycle('goto', 3);
  setActive($(this));
});

function setActive(clicked){
  $('.active').removeClass('active');
  $(clicked).addClass('active');
}
//hide mentions-content after cycle
$('.press-content').on('cycle-after',function(e, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag){
    var slideNum = $(this).data("cycle.opts").currSlide;
    if (slideNum == 0){$('.mentions-cycle').fadeIn(0);
    }else{$('.mentions-cycle').fadeOut(0);}
});


// PRESS MENTION BOX ACTIONS -----------------------
$('.mentions-box').click(function(){
  var logo = $(this).find('.m-logo').html();
  var text = $(this).find('.m-text').html();
  var link = $(this).find('a').attr('href');

  $('.h-logo').html(logo);
  $('.h-text').html(text);
  $('.mentions-hero a').attr("href", link)
  $('.mentions-hero').fadeIn();
});

$('.mentions-hero .close-button').click(function(){
  $('.mentions-hero').fadeOut();
});


// PRESS MEDIA IMAGES ACTIONS -----------------------
$('.media-ready-box').click(function(){
  var image = $(this).find('img').attr('src');
  var setBg = ('url("../'+ image + '")');
  $('.media-ready-hero').find('a').attr('href', image);
  $('.media-ready-hero').css('background-image', setBg);
  $('.media-ready-hero').fadeIn();
});

$('.media-ready-hero .close-button').click(function(){
  $('.media-ready-hero').fadeOut();
});

// NORTHPOLE TEMPERATURE -----------------------
var randTemp = Math.floor(Math.random()*(18-10+1)+10);
$('.npTemp').html('<span>-' + randTemp + '</span>&deg;');



// COUNTDOWN UNTIL CHRISTMAS -----------------------
var currentYear = (new Date).getFullYear();
var christmas = ("December 25, " + currentYear);
$('.countdown').countdown({
  date: christmas,
  render: function(data) {
    var el = this.leadingZeros(data.days, 3);
    var res = el.split("");
    $(".countdown").html("<div class='day'>"+res[0] + "</div><div class='day'>" + res[1]+"</div><div class='day'>"+res[2] + "</div>");
  }
});


// WHAT TO EXPECT QUESTIONS -----------------------
$('.faq-data p').click(function(){
  var question = $(this).text();
  var answer = $(this).parent().find('span').text();
  $('.answer').fadeOut(function(){
    $('.answer h3').text(question);
    $('.answer p').text(answer);
    $(this).fadeIn();
    if (windowWidth < 768){
      console.log('scroll to answer');
      $('html, body').animate({scrollTop: $('.answer-col').offset().top-150}, 800,'easeInOutExpo');
    }
  });
  
});


// BANNER ANIMATION -----------------------
//init video scrolling ================
  var sq = new SimpleSequencer({
    containerID:'banner',
    imgRoot: './ani/a/',
    nameRoot: 'npx_banner',
    namePrefix: '_A_',
    extension:'png',
    totalFrames: 164,
    bufferFrame: 164,
    stopFramesList: [0, 100],
    debug: false
  });
  sq.init();

  // sq.event.observe("progressupdate", function(e){
  //   //console.log("getting update");
  //   //console.log(e);
  //   console.log("loaded "+ e.percentLoaded +"%");
  // });

  sq.event.observe("loadingcomplete", function(){
    //console.log("LOADING IS DONE");
    sq.clickForward();
  });

  //fire event when selected step final frame is reached
  sq.event.observe("framecomplete", function(e){
    //console.log("sequence complete");
    loopBanner();
  });

  // sq.event.observe("frameupdate", function(e){
  //   console.log("cframe: "+ e.currentFrame);
  // });


function loopBanner(){
  //init video scrolling ================
  var sq = new SimpleSequencer({
    containerID:'banner',
    imgRoot: './ani/b/',
    nameRoot: 'npx_banner',
    namePrefix: '_B_',
    extension:'png',
    totalFrames: 72,
    bufferFrame: 72,
    stopFramesList: [0, 100],
    debug: false,
    loop:true
  });
  sq.init();

  // sq.event.observe("progressupdate", function(e){
  //   //console.log("getting update");
  //   //console.log(e);
  //   console.log("loaded "+ e.percentLoaded +"%");
  // });

  sq.event.observe("loadingcomplete", function(){
    //console.log("LOADING IS DONE");
    sq.clickForward();
  });

  //fire event when selected step final frame is reached
  sq.event.observe("framecomplete", function(e){
    //console.log("sequence complete");
  });

  // sq.event.observe("frameupdate", function(e){
  //   console.log("cframe: "+ e.currentFrame);
  // });
}

});//end