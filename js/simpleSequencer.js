
/*
* Title: SimpleSequencer
* Description: A tool to take a sequence if images and have it play back like a video
* using lo-res and high-res images. The lo-res to play inbetween and the high to display
*   whenever paused. Also has frame stepper feature which allows the user to set a list of frames
* to pause on whenever reached
* Author: Matt Murray [@mattanimation]
* Developed at: Kitchen Sink Studios
* Dependencies: uses a custom _utils.js file
* Usage Example:
      var sq = new SimpleSequencer({
          sliderId:'simple-slider',
          container:'imgb',
          imgRoot: './video/',
          nameRoot: 'island',
          lowName: '_low_',
          highName:'_high_',
          extension:'jpg',
          totalFrames: 150,
          stopFramesList: [0, 15, 45, 100, 110],
          debug: true
        });
        sq.init();

  Document needs something like the following for a slider:
    <div id="simple-slider" class="dragdealer">
        <div class="handle red-bar"><p class="value">drag me</p></div>
      </div>

*
*/

/*
* Todo: add easing, finish buffer chunks
*/

//import the utils file if not already imported
//aka load dependencies
if(Utils == null || Utils == undefined)
{
  // create a script tag
  // find the first script tag in the document
  // set the source of the script to your script
  // append the script to the DOM
  function loadScript(d, t) {
      var g = d.createElement(t),
          s = d.getElementsByTagName(t)[0];
      g.src = 'js/_utils.js'; 
      s.parentNode.insertBefore(g, s); 
  }

  loadScript(document, 'script');
}
else
  Utils.useDebug = true;


var SimpleSequencer = function(obj) {

  this.initRan = false;
  //these need to be set initially
  this.imgRoot = './video/';
  this.nameRoot = 'name';
  this.namePrefix = '_high_';
  this.extension = 'jpg';
  this.totalFrames = 150;
  this.fps = 30;
  this.percentLoaded = 0;
  this.isMobile = false;
  this.useStopFrames = false;
  this.bufferFrame = 100;
  this.debug = false;
  this.loadedCount = 0;
  this.loop =false;


  this.canPlay = false;
  this.currentImgInd = 0;
  this.currentTime = function(){return Utils.rightNow();};
  this.currentFrame = 0;
  this.delta = 0;
  this.frameNum = 0;
  this.frameToEndOn =0;
  this.lastFrame = 0;
  this.imgURLS =[];
  this.IMGS =[];
  this.currentImg = null;
  this.imgurl = "";
  this.containerID = null;
  this.container = null;
  this.autoplay = false;
  this.reverse = false;
  this.isScrolling = false;
  this.useHighRes = false;
  this.controls = {"stop":null, "play": null, "pause": null, "reverse": null, "frameStopBtns":[]};

  //list of frames to stop on
  this.currentStopFrameInd =0;

  //intervals
  this.playforwardinterval = null;
  this.playreverseinterval = null;
  this.preloadcheckinterval = null;
  this.animInterval = null;

  //for now end frame will be totalFrames
  this.frameToEndOn = this.totalFrames;

  if(obj !== null)
  {
    //set any default values
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        this[prop] = obj[prop];
      }
    }
  }

  //test utils debug
  //turn on
  //Utils.useDebug = true;
  //Utils.clog("barf!");
  //turn off
  //Utils.useDebug = false;
  //Utils.clog("no barf");

  Utils.useDebug = this.debug;

  //create custom events for external use
  this.event = new this.EventManager(this);
  /*
  add an addeventonframe event
  //custom events list
  //
  //progressupdate: fires whenever a file has finished loading an percent has been increased, returns percent
  //loadingcomplete: fires when all low images have been loaded
  //framecomplete: fires when a stepper value has been reached,
  */
  
  //loading complete
  //setInterval(function(){document.body.height = window.height},500);

};

SimpleSequencer.prototype.init =function(){

  this.initRan = true;
  Utils.clog("init ran: " +this.initRan);

  //reference to this for other scopes
  var oThis = this;
  this.isMobile = Utils.isMobile();
  this.container = document.getElementById(this.containerID);

  if(this.stopFramesList !== null)
    this.bufferFrame = this.stopFramesList[1];

  //if(this.controls.play == null && this.debug)
  //  alert("hey you haven't set up the controls silly");

  Utils.clog("start preload check...");
  this.preloadcheckinterval = setInterval(function(){
    oThis.preloadCheck();
  },100);

  this.preloadImages();
};

SimpleSequencer.prototype.preloadCheck = function(){
  //check to see if images have loaded
  var oThis = this;
  this.percentLoaded = Math.round((this.loadedCount/this.totalFrames) * 100);
  
  Utils.clog("checking progress...  percent = " + this.percentLoaded + " total is: " + this.totalFrames + " and loaded is: " + this.loadedCount);
  //$('#preloader').html("loaded "+ this.percentLoaded +"%");
  //fire off event
  var perc = this.percentLoaded;
  this.event.fire("progressupdate", {percentLoaded:perc});

  if(this.loadedCount === this.bufferFrame || this.percentLoaded >= 100)
  {

    Utils.clog("all images loaded");
    $('#preloader').html("done loading");
    //completion event fire
    Utils.clog("should load high");

    this.canPlay = true;
    //get rid of checking interval
    clearInterval(this.preloadcheckinterval);

    if(this.autoplay)
      playImageSequenceForward();

    this.event.fire("loadingcomplete");

  }
};


SimpleSequencer.prototype.preloadImages = function(){

  Utils.clog("Pre-load images started.");
  //build the img urls
  for(var i=0; i < this.totalFrames; i++)
  {
    this.imgurl = this.imgRoot + this.nameRoot + this.namePrefix + Utils.paddit(i) + '.' + this.extension;
    Utils.clog("url: " + this.imgurl);
    this.imgURLS[i] = this.imgurl;
  }
  
  //load all the images
  for(var i=0; i < this.imgURLS.length; i++)
  {
    Utils.clog("load image "+ Utils.paddit(i));
    this.IMGS[i] = this.loadImage(this.imgURLS[i], this);
  }

};


SimpleSequencer.prototype.loadImage = function(src, oThis){
  var img = new Image();
  img.src = src;
  var dis = oThis;
  img.addEventListener("load", function(){dis.checkImageLoad(this);}, false);
  return img;
};

SimpleSequencer.prototype.checkImageLoad = function(disImg){
  Utils.clog("loaded " + disImg.src + " loadedCount: " + this.loadedCount);
  this.loadedCount++;
};

SimpleSequencer.prototype.playImageSequenceForward = function (){
  //option using set Interval

  if(this.canPlay && this.initRan)
  {
    this.clearForwardReverseIntervals();
    var oThis = this;
    this.playforwardinterval = setInterval(function(){
      oThis.playForward();
    },1);
  }
  else
    alert("either images are not preloaded or you forgot to run init");
};

SimpleSequencer.prototype.playForward = function(){
  this.setFrame(this.calculateFrame(false), false);
};

SimpleSequencer.prototype.playImageSequenceReverse = function(){
  if(this.canPlay)
  {
    this.clearForwardReverseIntervals();
    var oThis = this
    this.playreverseinterval = setInterval(function(){
      oThis.playReverse();
    },1);
  }
  else
    alert("either images are not preloaded or you forgot to run init");
};

SimpleSequencer.prototype.playReverse = function(){

  this.setFrame(this.calculateFrame(true), true);
  
};

SimpleSequencer.prototype.pauseSequence = function(){
  //pause on current frame
  if(this.canPlay)
  {
    this.clearForwardReverseIntervals();
    this.frameToEndOn = this.currentImgInd;

  }
};

SimpleSequencer.prototype.stopSequence = function(){
  //stop and return to beginning
  if(this.canPlay)
  {
    this.pauseSequence();
    this.resetSequence();
  }
};

//reset to beginning frame
SimpleSequencer.prototype.resetSequence = function(){
  if(this.canPlay)
  {
    this.frameToEndOn = this.frameNum = this.currentFrame = 0;
    this.currentImgInd = this.frameToEndOn;
    this.currentTime = Utils.rightNow();
    this.setFrame(0, true);
  }
};


SimpleSequencer.prototype.setFrame = function(framenum, goReverse) {

  //basically don't scroll the video until the minimum offset has
  //been passed

  this.reverse = goReverse;
  this.frameNum = framenum;

  var oThis = this;
  //clamp the frame number to not exceed min or max values
  this.frameNum = Utils.clamp(oThis.frameNum,0, this.totalFrames);
  this.frameToEndOn = Utils.clamp(oThis.frameToEndOn, 0, this.totalFrames);

  this.currentImgInd = this.frameNum;
  Utils.clog(" current frame is: " + this.currentImgInd + " img url is " + this.imgURLS[this.currentImgInd]);


  if(this.frameNum <= this.totalFrames-1 && this.currentFrame >= 0)
  {
    this.currentLowImg = this.imgURLS[this.currentImgInd];

    try{
      this.container.src = this.currentLowImg;
      this.event.fire("frameupdate", {currentFrame: oThis.currentFrame, totalFrames: oThis.totalFrames});

    }catch(err){
      console.log("ended with error... " + err);
      this.clearForwardReverseIntervals();
    }
  }
  else
  {
    if(this.loop)
    {
      
      this.currentFrame = 0;
      this.currentImgInd = 0;
      if(this.reverse)
      {
        this.currentFrame = this.totalFrames-1;
        this.currentImgInd = this.totalFrames-1;
      }
      this.event.fire("frameloop", {currentFrame: oThis.currentFrame, totalFrames: oThis.totalFrames});
    }
    else
    {
      this.event.fire("frameupdate", {currentFrame: oThis.currentFrame, totalFrames: oThis.totalFrames});
      this.event.fire("framecomplete", {currentFrame: oThis.currentFrame, totalFrames: oThis.totalFrames});
      this.clearForwardReverseIntervals();
      this.currentImgInd = this.frameToEndOn;
    }
    
  }

  this.currentTime = Utils.rightNow();
  
};


SimpleSequencer.prototype.clearForwardReverseIntervals = function(){
  //make sure the intervals controlling the frames don't interfere with each other
  if(this.playforwardinterval != null || this.playforwardinterval != undefined)
    clearInterval(this.playforwardinterval);
  if(this.playreverseinterval != null || this.playreverseinterval != undefined)
    clearInterval(this.playreverseinterval);
};

//============UTILS======================

SimpleSequencer.prototype.EventManager = function(target) {
    var target = target || window,
        events = {};
    this.observe = function(eventName, cb) {
        if (events[eventName]) events[eventName].push(cb);
        else events[eventName] = new Array(cb);
        return target;
    };
    this.stopObserving = function(eventName, cb) {
        if (events[eventName]) {
            var i = events[eventName].indexOf(cb);
            if (i > -1) events[eventName].splice(i, 1);
            else return false;
            return true;
        }
        else return false;
    };
    this.fire = function(eventName) {
        if (!events[eventName]) return false;
        for (var i = 0; i < events[eventName].length; i++) {
            events[eventName][i].apply(target, Array.prototype.slice.call(arguments, 1));
        }
    };
};

SimpleSequencer.prototype.getCurrentStopFrameInd = function(){
  return this.currentStopFrameInd;
};

SimpleSequencer.prototype.calculateFrame = function(dir){

  this.delta = (Utils.rightNow()- this.currentTime) / 1000;

  if(!dir)
    this.currentFrame += (this.delta * this.fps);
  else
    this.currentFrame -= (this.delta * this.fps);

  return Math.floor(this.currentFrame);
};

SimpleSequencer.prototype.clickForward = function(){
  this.frameToEndOn =this.totalFrames;
  this.currentTime = Utils.rightNow();
  this.playImageSequenceForward();
};

SimpleSequencer.prototype.clickReverse = function(){
  this.frameToEndOn =0;
  this.currentTime = Utils.rightNow();
  this.playImageSequenceReverse();
};







