<header>
  <div class="left-border"></div>
  <div class="right-border"></div>
  <div class="title">
    <div class="tickets">
      <p>BUY <img src="img/header-ticket.svg"> HERE</p>
    </div>

    <div class="logo">
      <img src="img/header-logo.svg">
    </div>
    <div class="logo-small">
      <img src="img/header-logo-small.svg">
    </div>

    <div class="social">
      <div class="facebook">
        <img src="img/header-facebook.svg">
      </div>
      <div class="twitter">
        <img src="img/header-twitter.svg">
      </div>
      <div class="instagram">
        <img src="img/header-instagram.svg">
      </div>
      <div class="pintrest">
        <img src="img/header-pintrest.svg">
      </div>
    </div>
  </div>


  <?php include('nav.php') ?>

</header>