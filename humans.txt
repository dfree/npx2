/* the humans responsible & colophon */
/* humanstxt.org */


/* TEAM */
  Kitchen Sink Studios
  kitchensinkstudios.com
  
  Design: Designy McAwesome
  Development: Dan Freeman [@dFree]
  


/* SITE */
  Standards: HTML5, CSS3 
  Components: Modernizr, jQuery, Normalize
